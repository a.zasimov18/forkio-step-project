;(function() {
    const menu = document.querySelector('.js-menu');
    const overlay = document.querySelector('.js-overlay');
    const icon = document.querySelector('.js-icon');
    let isOpen = false;

    if(overlay) overlay.addEventListener('click', function(){
        menu.classList.remove('nav_active');
        this.src = "img/burger-menu.svg";
        overlay.classList.remove('active');
        isOpen = false;
    });
    if(icon) icon.addEventListener('click', function(){
        isOpen = !isOpen;
        if(isOpen) {
            menu.classList.add('nav_active');
            overlay.classList.add('active');
            this.src = "img/close.svg";
        } else {
            menu.classList.remove('nav_active');
            this.src = "img/burger-menu.svg";
            overlay.classList.remove('active');
        }
    });
})(); 